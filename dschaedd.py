#!/bin/python

import pytchat
import asyncio
import websockets
import json
import sys
import time
from signal import signal, SIGINT
from sys import exit
from pytube import YouTube

if len(sys.argv) < 2:
    stream = "testlink"
else:
    stream = sys.argv[1].split("/watch?v=")[1]
    stream_url = sys.argv[1]


last_view_query = 0

def unix():
    return int(time.time())


async def handle_message(ws, c):
    global last_view_query
    if ws is None:
        return

    data = {}
    data["user"] = c.author.name
    data["owner"] = c.author.isChatOwner
    data["mod"] = c.author.isChatModerator
    data["avatar_url"] = c.author.imageUrl
    data["badge_url"] = c.author.badgeUrl

    if unix() - last_view_query > 30:
        last_view_query = unix()
        try:
            yt = YouTube(stream_url)
            if yt:
                data["views"] = yt.views

        except:
            print("Error when getting views")

    if c.bgColor is not None:
        data["bg"] = hex(c.bgColor)
        data["time"] = c.timestamp
    if c.type == "superChat":
        data["type"] = "donation"
        data["cash"] = c.amountValue
        data["cash_str"] = c.amountString
        data["currency"] = c.currency
        data["text"] = c.message
        data["all_text"] = c.messageEx
    elif c.type == "superSticker":
        data["type"] = "donation"
        data["cash"] = c.amountValue
        data["cash_str"] = c.amountString
        data["currency"] = c.currency
    elif c.type == "textMessage":
        data["type"] = "message"
        data["text"] = c.message
        data["all_text"] = c.messageEx
    elif c.type == "newSponsor":
        data["type"] = "member"
        data["text"] = c.message
    

    print(f"{c.author.name}: {c.message}")
    await ws.send(json.dumps(data))


async def echo(websocket, path):
    print("Received WebSocket connection")
    chat = pytchat.create(video_id=stream)
    await websocket.send(json.dumps({
        "type": "start"
    }))
    while chat.is_alive() and websocket.open:
        for c in chat.get().sync_items():
            await handle_message(websocket, c)

def handler(signal_received, frame):
    print('SIGINT or CTRL-C detected. Exiting gracefully')
    exit(0)

async def server():
    async with websockets.serve(echo, "localhost", 1510):
        await asyncio.Future()

if __name__ == '__main__':
    signal(SIGINT, handler)

    print('Running. Press CTRL-C to exit.')
    asyncio.run(server())
